<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- <link rel="shortcut icon" href="images/favicon.png"> -->
    <title>Nailcrust Store</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="vendor/animateit/animate.min.css" rel="stylesheet">

    <!-- Vendor css -->
    <link href="vendor/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Template base -->
    <link href="css/theme-base.css" rel="stylesheet">

    <!-- Template elements -->
    <link href="css/theme-elements.css" rel="stylesheet">

    <!-- Responsive classes -->
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Template color -->
    <link href="css/color-variations/purple.css" rel="stylesheet" type="text/css" media="screen">

    <!-- LOAD GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

    <!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" type="text/css" href="css/custom.css" media="screen" />

    <!-- FONTS -->
    <link rel="stylesheet" type="text/css" href="css/fonts.css" media="screen" />

    <!--VENDOR SCRIPT-->
    <script src="vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="vendor/plugins-compressed.js"></script>

</head>

<body class="wide">

    <!-- WRAPPER -->
    <div class="wrapper">


        <!-- Header -->

        <!-- END: Header -->

        <!-- Slider with Particles -->
		<section class="fullscreen background-image" style="background-image:url(https://scontent-sin6-1.xx.fbcdn.net/v/t31.0-8/14409901_189186731507470_6160113218693683299_o.jpg?oh=a8fab7ff2b645e5c6f5d14726f020d1b&oe=59B688C6);">
			<div class="container">
				<div class="container-fullscreen">
					<div class="text-middle text-center text-light">
						<!-- Captions -->
<h1>NAILCRUST STORE </h1>
<p>UNHOLLY STORE Jln. Panglima Sudirman no. 42 caruban (depan terminal caruban)</p>
<p> <i class="fa fa-instagram"></i> Instagram : <a href="https://www.instagram.com/nailcrust_store/" target='_blank'>@nailcrust_store</a> <br>
<i class="fa fa-facebook"></i> Facebook : <a href="https://www.facebook.com/nailcrust/" target='_blank'>@nailcrust</a></p>
                            <!-- End: Captions -->
					</div>
				</div>
			</div>
            <!-- Particles Scripts -->
            <div id="particles-js" class="particles"></div>
            <script src="vendor/partical/particles.min.js" type="text/javascript"></script>
            <script src="vendor/partical/partical-animation.js" type="text/javascript"></script>
		</section>
        <!-- END:  Slider with Particles -->

        <!-- Our numbers -->

        <!-- END: Our numbers -->

        <!-- About us -->


        <!-- END: REVIEWS -->

        <!-- Client logo -->
      </div>
    <!-- End: Wrapper -->

    <!-- Theme Base, Components and Settings -->
    <script src="js/theme-functions.js"></script>

    <!-- Custom js file -->
    <script src="js/custom.js"></script>

</body>

</html>
